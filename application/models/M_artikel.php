<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_artikel extends CI_Model
{

    var $column_order = array(null, 'a.judul_karya','a.kategori_id','a.isi_karya','a.url_video','a.status','a.user_id'); //set column field database for datatable orderable
    var $column_search = array( 'a.judul_karya','a.kategori_id','a.isi_karya','a.url_video','a.status','a.user_id'); //set column field database for datatable searchable
    var $order = array('a.id_karya' => 'asc'); // default order 

    private function _get_datatables_query()
    {
        $this->db->select('a.*, b.nama_kategori as kategori, c.nama_alias as author');
        $this->db->from('kp_karya a');
	    $this->db->join('kp_kategori b', 'b.id_kategori = a.kategori_id ', 'left');
        $this->db->join('kp_user c', 'c.id_user = a.user_id ', 'left');
        // $this->db->join('kp_karya_komentar d', 'd.karya_id = a.id_karya ', 'left');
        $this->db->where('a.deleted_at IS NULL');
        $this->db->order_by('a.id_karya','desc');
        $i = 0;
        foreach ($this->column_search as $data) { // loop column 
            if (@$_POST['search']['value']) { // if datatable send POST for search
                if ($i === 0) { // first loop
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($data, $_POST['search']['value']);
                } else {
                    $this->db->or_like($data, $_POST['search']['value']);
                }
                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables()
    {
        $this->_get_datatables_query();
        if (@$_POST['length'] != -1)
            $this->db->limit(@$_POST['length'], @$_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    function count_all()
    {
        $this->db->from('kp_karya');
       
        return $this->db->count_all_results();
    }
    // end datatables


	function get_all_data()
	{		

		return $this->db->query("SELECT a.*, b.nama_kategori as kategori
		FROM kp_karya a
		INNER JOIN kp_kategori b ON b.id_kategori = a.kategori_id 
		WHERE a.deleted_at IS NULL
		")->result();
	}
	 public function get_komen($id = null)
    {
         return $this->db->query("SELECT a.*, COUNT(b.karya_id) as komentar
		FROM kp_karya a
		INNER JOIN kp_karya_komentar b ON b.karya_id = a.id_karya 
		WHERE a.id_karya = '$id' AND a.deleted_at IS NULL
		")->result();
    }
     public function get($id = null)
    {
        $this->db->select('a.*, b.nama_kategori as kategori');
        $this->db->from(' kp_karya a');
        $this->db->join('kp_kategori b', 'b.id_kategori = a.kategori_id  ' ,'left');
        $this->db->where('a.deleted_at IS NULL');
        if ($id != null) {
            $this->db->where('a.id_karya', $id );
            
        }
        $query = $this->db->get();
        return $query;
        # code...
    }

public function getall($id = null)
    {
        
        $this->db->from('kp_karya');
        
        if ($id != null) {
            $this->db->where('id_karya', $id);
        }
        $query = $this->db->get();
        return $query;
        # code...
    }
    
	function insert_post($judul,$isi,$kategori,$status,$author){
		$data = array(
            'user_id'    => $author,
			'judul_karya'    => $judul,
			'isi_karya' => $isi,
            'kategori_id' => $kategori,
            'status' => $status,
			'created_at'=>date('Y-m-d H:i:s'),
		);
		$this->db->insert('kp_karya',$data);
	}

	function get_article_by_id($id){
		$query = $this->db->get_where('kp_karya', array('id_karya' =>  $id));
		return $query;
	}

public function edit() {
        $id = $this->input->post('id_karya', true);
         
        $data = array(
                        'judul_karya' =>$this->input->post('judul_karya'),
                        'isi_karya'=>$this->input->post('isi_karya'),
                       
                        'updated_at'=>date('Y-m-d H:i:s'),
                      
                    );
         $this->db->where('id_karya', $id); 
    
        $this->db->update('kp_karya',$data); 

	
	}

	 public function del($id)
    {
        $data = array(
            'deleted_at' => date('Y-m-d H:i:s')
            );
        $this->db->where('id_karya', $id);
        $this->db->update('kp_karya',$data);
    }
}
