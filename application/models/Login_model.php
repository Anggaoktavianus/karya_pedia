<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login_model extends CI_Model
{
	public function get_kategori(){
	$sql =
            " SELECT * FROM `kp_kategori` ";

        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {
            $data = $query->result();
        } else {
            $data = array();
        }

        return $data;

	}

	public function get_role(){
	$sql =
            " SELECT * FROM `kp_role` WHERE id_role NOT IN (1,2,3,4,5,6) ";

        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {
            $data = $query->result();
        } else {
            $data = array();
        }

        return $data;

    }

    public function m_cek_role() {

     return $this->db->get_where('kp_role',array('id_role' => 1,'status' => 1));

    }

     public function m_cek_mail() {

     return $this->db->get_where('kp_user',array('email' => $this->input->post('email'),'status' => 1));

    }
     
    public function m_cek_mails() {

     return $this->db->get_where('kp_user',array('email' => $this->input->post('email'),'status' => 0));

    }
}