<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_kategori extends CI_Model
{

    var $column_order = array(null, 'a.nama_kategori'); //set column field database for datatable orderable
    var $column_search = array( 'a.nama_kategori'); //set column field database for datatable searchable
    var $order = array('a.id_kategori' => 'asc'); // default order 

    private function _get_datatables_query()
    {
        $this->db->select('a.* ');
        $this->db->from('kp_kategori a');
        $this->db->where('a.deleted_at IS NULL');
        $this->db->order_by('a.id_kategori','desc');
        $i = 0;
        foreach ($this->column_search as $data) { // loop column 
            if (@$_POST['search']['value']) { // if datatable send POST for search
                if ($i === 0) { // first loop
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($data, $_POST['search']['value']);
                } else {
                    $this->db->or_like($data, $_POST['search']['value']);
                }
                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables()
    {
        $this->_get_datatables_query();
        if (@$_POST['length'] != -1)
            $this->db->limit(@$_POST['length'], @$_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    function count_all()
    {
        $this->db->from('kp_kategori');
       
        return $this->db->count_all_results();
    }
    // end datatables

	function get_all_data()
	{		

		return $this->db->query("SELECT a.*
		FROM kp_kategroi a
		WHERE a.deleted_at IS NULL
		")->result();
	}
	 public function get($id = null)
    {
        $this->db->select('a.*');
        $this->db->from(' kp_kategori a');
        $this->db->where('a.deleted_at IS NULL');
        if ($id != null) {
            $this->db->where('a.id_kategori', $id );
            
        }
        $query = $this->db->get();
        return $query;
        # code...
    }

public function getall($id = null)
    {
        
        $this->db->from('kp_kategori');
        
        if ($id != null) {
            $this->db->where('id_kategori', $id);
        }
        $query = $this->db->get();
        return $query;
        # code...
    }
    
	function insert($nama_kategori){
		$data = array(
            'nama_kategori' => $nama_kategori,
			'created_at'=>date('Y-m-d H:i:s'),
		);
		$this->db->insert('kp_kategori',$data);
	}


public function edit() {
        $id = $this->input->post('id_kategori', true);
         
        $data = array(
                        'nama_kategori' =>$this->input->post('nama_kategori'),
                       
                        'updated_at'=>date('Y-m-d H:i:s'),
                      
                    );
         $this->db->where('id_kategori', $id); 
    
        $this->db->update('kp_kategori',$data); 

	
	}

	 public function del($id)
    {
        $data = array(
            'deleted_at' => date('Y-m-d H:i:s')
            );
        $this->db->where('id_kategori', $id);
        $this->db->update('kp_kategori',$data);
    }
}
