<?php

class Konfigurasi_model extends Ci_Model {

	var $data;
	
	function get_all_data()
	{		
		return $this->db->query("SELECT * FROM konfigurasi")->result();
	}

	function get_group()
	{
		$this->db->select('group');
		$this->db->group_by('group');
		$this->db->order_by('order','ASC');
		$this->db->where('status','active');
		return $this->db->get('konfigurasi');
	}
	
	function get_config_group($group)
	{
		$this->db->where('group',$group);
		$this->db->order_by('order','ASC');
		$this->db->where('status','active');
		return $this->db->get('konfigurasi'); 
		
	}
	
	function get_config()
	{
		$this->db->order_by('urut', 'ASC');
		return $this->db->get('konfigurasi');
		
	}
	
	function get_config_id($id)
	{
		$this->db->where('id', $id);
		return $this->db->get('konfigurasi');
	}

	function update_value($id,$value)
	{
		$this->data['value'] = $value;
		$this->db->where('id',$id);
		$update = $this->db->update('konfigurasi', $this->data);
		return $update;
		
	}
	
	function update($id)
	{
		$this->data['value'] = $this->input->post('value_'.$id);
		$this->db->where('id',$id);
		$update = $this->db->update('konfigurasi', $this->data);
		return $update;
		
	}

}
?>