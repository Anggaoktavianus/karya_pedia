<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kategori extends CI_Controller
{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct()
	{
		parent::__construct();
		// not_login();
		$this->load->model('M_kategori');
		$this->load->model('M_user');
		$this->load->library('upload');
		
		
	}

	function get_ajax()
    	{
		$list = $this->M_kategori->get_datatables();
		$data = array();
		$no = @$_POST['start'];
		foreach ($list as $a) {
		$no++;
		$row = array();
		$row[] = $no . ".";
		$row[] = $a->nama_kategori;
		
		$row[] = '<form action="' .site_url('kategori/del').'" method="post"><a href="' . site_url('kategori/act_edit/' . $a->id_kategori) . '" class="btn bg-primary btn-icon rounded-round btn-sm" "><i class="fas fa-pencil-alt"></i></a>
			<input type="hidden" name="id" value="'. $a->id_kategori.'"> <button onclick="return confirm(\'Yakin hapus data?\')" class="btn bg-danger btn-icon rounded-round btn-sm" >
				<i class="fas fa-trash-alt "></i>
				</button></form>';
		$data[] = $row;
		}
		$output = array(
		"draw" => @$_POST['draw'],
		"recordsTotal" => $this->M_kategori->count_all(),
		"recordsFiltered" => $this->M_kategori->count_filtered(),
		"data" => $data,
		);
		// output to json format
		echo json_encode($output);
    	}

	public function index()
	{
		
		
		
		// $this->template->load('template', 'backend/dashboard');
		$this->load->view('template/headx');
		$this->load->view('template/header');
		$this->load->view('template/sidebar');
		$this->load->view('kategori/v_kategori');   
		// $this->load->view('template/footerx');   
	}

	function add ()
	{
		$kategori=$this->M_user->get_kategori();
		$data = array(
					'kategori' => $kategori,

				);
		$this->load->view('template/headx');
		$this->load->view('template/header');
		$this->load->view('template/sidebar');
		$this->load->view('kategori/add_kategori',$data);  
	}

	function act_add(){

		
		$kategori = $this->input->post('kategori',TRUE);
		$this->M_kategori->insert($kategori);
		

		$this->session->set_flashdata('info', 'success');
          	$this->session->set_flashdata('pesan', 'Berhasil menambah data.');
           	redirect('kategori','refresh');
	}




	function act_edit($id){

		$this->form_validation->set_rules('id_kategori', 'Id', 'trim|required');

		$this->form_validation->set_error_delimiters('<span class="help-block">', '</span>');

		if ($this->form_validation->run() == FALSE) {
			 $query = $this->M_kategori->getall($id);
			 $kategori=$this->M_user->get_kategori();
			if ($query->num_rows() > 0) {
				$row = $query->row();
				
				 $data = array(
					'row' => $row,
					'kategori' => $kategori,

				);
				$this->load->view('template/headx');
				$this->load->view('template/header');
				$this->load->view('template/sidebar');
				$this->load->view('kategori/edit_kategori',$data);   
				
			} else {
				echo "<script>alert ('Data tidak ditemukan');";
				echo "window.location ='" . site_url('kategori') . "' ; </scrip>";
			}
		} else {
		$this->M_kategori->edit();
		 if($this->db->affected_rows() > 0){
		$this->session->set_flashdata('info', 'success');
          	 $this->session->set_flashdata('pesan', 'Berhasil merubah data.');
           	redirect('kategori','refresh');
		}
		echo "<script>window.location ='" . site_url('kategori') . "' ; </script>";
		}
	}

public function del()
	{
		$id = $this->input->post('id');
		// $this->m_produksi->del($id);

		$row = $this->M_kategori->get($id);

		if ($row) {
		$this->M_kategori->del($id);
		
		$this->session->set_flashdata('info', 'success');
			$this->session->set_flashdata('pesan', 'Data berhasil dihapus');
			redirect(site_url('kategori'));
		} else {
		$this->session->set_flashdata('info', 'danger');
		$this->session->set_flashdata('pesan', 'gagal hapus ');
		redirect(site_url('kategori'));
		}
	}	


	
}
