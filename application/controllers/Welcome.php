<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	 function __construct()
	{
		parent::__construct();


		$this->load->model(['m_user']);
		
	}
	public function index()
	{
		$row = $this->m_user->get_kategori();
		$data = array(
		'row' => $row,
		

		);
		$this->load->view('template/head',$data);
		$this->load->view('frontend/index');
		$this->load->view('template/footer');
	}

	public function mgmp()
	{
		$row = $this->m_user->get_kategori();
		$data = array(
		'row' => $row,
		

		);
		$this->load->view('template/head',$data);
		$this->load->view('frontend/mgmp');
		$this->load->view('template/footer');
	}
	public function contact()
	{
		$row = $this->m_user->get_kategori();
		$data = array(
		'row' => $row,
		

		);
		$this->load->view('template/head',$data);
		$this->load->view('frontend/contact');
		$this->load->view('template/footer');
	}
}
