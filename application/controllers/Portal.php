<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Portal extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct()
     {
        parent::__construct();
		$this->load->library('authentification');
		$this->load->library('session');
  	 }

	public function index(){        
        // $this->load->view('login');  
	$this->load->view('frontend/login');   
	}

	public function auth(){
		$s = $this->input->post('satu');
		$d = $this->input->post('dua');
		$j = $this->input->post('jawaban');

		$k = $s + $d;
		if($k == $j){
			$data = array(
				'email'=>$this->input->post('email'),
				'password'=>$this->input->post('password')
			);
			$log = $this->authentification->login($data);
			if($log == FALSE){
				$this->session->set_flashdata('info', 'danger');
				$this->session->set_flashdata('message', 'Username / Password Salah!');
				redirect('portal');
			}else{
				$this->session->set_flashdata('info', 'success');
				$this->session->set_flashdata('message', 'Halo, Selamat Datang Kembali di Karyapedia');
				redirect('dashboard');
			}
		}else{
			$this->session->set_flashdata('info', 'danger');
				$this->session->set_flashdata('message', 'Captcha Salah!');
				redirect('portal');
		}
		
	}

	public function logOut(){        
        $this->session->sess_destroy();
        redirect('portal');
    }



}
