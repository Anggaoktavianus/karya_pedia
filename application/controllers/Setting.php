<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Setting extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        // $this->authentification->check_user_authentification();

        $this->load->model('Note_model');
        // $this->load->model('Konfigurasi_model');
        $this->load->model('Privilege_model');
        //testing
    }

    public function index()
    {
        redirect('dashboard');
    }

    public function privilege()
    {
        // <a  href="' . base_url('setting/privilage_edit/') . $value->id . '" type="button" class="btn bg-primary btn-icon rounded-round"><i class="fa fa-edit"></i></a> &nbsp;
        $get = $this->Privilege_model->get_all_data();
        $datatable = array();
        foreach ($get as $key => $value) {
           

            if ($value->status == 1) {
                $status = "<span class='badge badge-success'>Aktif</span>";
            } else {
                $status = "<span class='badge badge-danger'>Tidak Aktif</span>";
            }

            // $action =
            // '<div class="row">
            // <div class="row">
            // <a type="button" class="btn bg-primary btn-icon rounded-round" href="javascript:;"
            // data-id="'.$value->id.'"
            // data-nama="'.$value->name.'"
            // data-status="'.$value->status.'"
            // data-deskripsi="'.$value->description.'""
            // data-toggle="modal" data-target="#edit-data"
            // >
            // <i class="fa fa-edit"></i>
            // </a>&nbsp;

            // <a  onclick="return confirm(\'Are you sure you want to delete this item?\');" href="' . base_url('setting/privilage_deleted/') . $value->id . '" type="button" class="btn bg-warning btn-icon rounded-round"><i class="fa fa-trash"></i></a>
    
            // </div>';
             $action =
            '<div class="row">
            <div class="row">
           

            <a  href="' . base_url('setting/privilege_edit/') . $value->id . '" type="button" class="btn bg-primary btn-icon rounded-round"><i class="fa fa-users"></i></a>&nbsp;
            <form action="' .site_url('setting/privilege_del').'" method="post">
            <input type="hidden" name="id" value="'. $value->id.'"> <button onclick="return confirm(\'Yakin hapus data?\')" class="btn bg-danger btn-icon rounded-round btn-xs" >
				<i class="fas fa-trash-alt "></i>
				</button></form>
    
            </div>';


            $datatable[$key] = array(
                'name'            => $value->name,
                'description'     => $value->description,
                'status'          => $status,
                'action'          => $action
            );
        }
        $data['datatable'] = $datatable;

        $this->load->view('template/headx');
        $this->load->view('template/header');
        $this->load->view('template/sidebar');
        $this->load->view('setting/privilege/privilege_index', $data);
    }
     public function privilege_edit($id)
    {
        $data['form_data'] = $form_data = $this->Privilege_model->get_data_by_id($id)->row_array();
        $data['access'] = explode(",",$form_data['access']);
		$data['module_data'] = $module_data = $this->Privilege_model->get_kp_module('0','0');
        $this->load->view('template/headx');
        $this->load->view('template/header');
        $this->load->view('template/sidebar');
        $this->load->view('setting/privilege/privilege_edit',$data);
    }

    public function privilege_update($id)
    {
        $this->Privilege_model->fill_data();
        if(!$this->Privilege_model->check_id($id))
        {
            $this->session->set_flashdata('info', 'success');
			$this->session->set_flashdata('message', 'Id Sudah Digunakan');
            redirect('setting/privilege_edit/'.$id);
            // echo json_encode(array('success'=>false,'message'=>"ID telah digunakan"));
        }
        elseif($this->Privilege_model->update_data($id))
        {
            $this->session->set_flashdata('info', 'success');
			$this->session->set_flashdata('message', 'Berhasil Update Data');
            redirect('setting/privilege_edit/'.$id);
            // echo json_encode(array('success'=>true,'message'=>"Data Hak Akses Berhasil Diubah"));
        }
        else
        {
            $this->session->set_flashdata('info', 'danger');
			$this->session->set_flashdata('message', 'Gagal Update Data');
            redirect('setting/privilege_edit/'.$id);
            // echo json_encode(array('success'=>false,'message'=>"Data Hak Akses Gagal diubah"));
        }
    }

    // public function konfigurasi()
    // {

    //     $data['config'] = $this->Konfigurasi_model->get_all_data();
    //     $this->load->view('template/head');
    //     $this->load->view('template/header');
    //     $this->load->view('template/sidebar');
    //     $this->load->view('setting/konfigurasi/konfigurasi_index',$data);
    // }

    // public function konfigurasi_edit_act()
    // {
    //     $config_data = $this->Konfigurasi_model->get_config();
    //     foreach ($config_data->result() as $config)
	// 		{	
	// 			$this->Konfigurasi_model->update($config->id);
	// 		}
    //     redirect('setting/konfigurasi');
    // }

    public function privilege_del()
	{
		$id = $this->input->post('id');
		// $this->m_produksi->del($id);

		$row = $this->Privilege_model->get($id);

		if ($row) {
		$this->Privilege_model->del($id);
		
		$this->session->set_flashdata('info', 'success');
			$this->session->set_flashdata('pesan', 'Data berhasil dihapus');
			redirect(site_url('setting/privilege'));
		} else {
		$this->session->set_flashdata('info', 'danger');
		$this->session->set_flashdata('pesan', 'gagal hapus ');
		redirect(site_url('setting/privilege'));
		}
	}	
    public function note()
    {

        $data['notes'] = $this->Note_model->get_note()->result();
        // echo json_encode($data);
        $this->load->view('template/head');
        $this->load->view('template/header');
        $this->load->view('template/sidebar');
        $this->load->view('setting/note/note_index',$data);
    }

    public function note_edit_act()
    {
        foreach ($this->Note_model->get_note()->result() as $note)
			{	
				$this->Note_model->update($note->id);
			}
        redirect('setting/note');
    }

    

    
}
