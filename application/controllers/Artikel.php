<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Artikel extends CI_Controller
{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct()
	{
		parent::__construct();
		// not_login();
		$this->load->model('M_artikel');
		$this->load->model('M_user');
		$this->load->library('upload');
		
		
	}

	function get_ajax()
    	{
		$list = $this->M_artikel->get_datatables();
		
		$data = array();
		
		$no = @$_POST['start'];
		foreach ($list as $a) {
		$no++;
		$row = array();
		$row[] = $no . ".";
		$row[] = $a->judul_karya;
		$row[] = $a->kategori;
		$row[] = $a->author;
		$row[] = $a->created_at;
		$row[] = ' <i class="fas fa-comment-alt btn btn-icon rounded-round btn-sm " data-toggle="modal" data-target="#myModal">&nbsp;'.$a->komentar.'</i>';
		
		$row[] = $a->status;
		// $row[] =  $a->status;
		// add html for action
		$row[] = '<form action="' .site_url('artikel/del').'" method="post"><a href="' . site_url('artikel/act_edit/' . $a->id_karya) . '" class="btn bg-primary btn-icon rounded-round btn-sm" "><i class="fas fa-pencil-alt"></i></a>
			<input type="hidden" name="id_karya" value="'. $a->id_karya.'"> <button onclick="return confirm(\'Yakin hapus data?\')" class="btn bg-danger btn-icon rounded-round btn-sm" >
				<i class="fas fa-trash-alt "></i>
				</button></form>';
		$data[] = $row;
		}
		$output = array(
		"draw" => @$_POST['draw'],
		"recordsTotal" => $this->M_artikel->count_all(),
		"recordsFiltered" => $this->M_artikel->count_filtered(),
		"data" => $data,
		);
		// output to json format
		echo json_encode($output);
    	}

	public function index()
	{
		
		
		
		// $this->template->load('template', 'backend/dashboard');
		$this->load->view('template/headx');
		$this->load->view('template/header');
		$this->load->view('template/sidebar');
		$this->load->view('artikel/v_artikel');   
		// $this->load->view('template/footerx');   
	}

	function add ()
	{
		$kategori=$this->M_user->get_kategori();
		$data = array(
					'kategori' => $kategori,

				);
		$this->load->view('template/headx');
		$this->load->view('template/header');
		$this->load->view('template/sidebar');
		$this->load->view('artikel/add_artikel',$data);  
	}

	function act_add(){

		$author = $this->input->post('author',TRUE);
		$judul = $this->input->post('judul',TRUE);
		$isi = $this->input->post('isi',TRUE);
		$kategori = $this->input->post('kategori',TRUE);
		$status = $this->input->post('status',TRUE);
		$this->M_artikel->insert_post($judul,$isi,$kategori,$status,$author);


		// $id = $this->db->insert_id();
		// $result = $this->M_artikel->get_article_by_id($id)->row_array();

		// $data['judul'] = $result['judul'];
		// $data['isi'] = $result['isi'];
		

		$this->session->set_flashdata('info', 'success');
          	$this->session->set_flashdata('pesan', 'Berhasil menambah data.');
           	redirect('artikel','refresh');
	}


	function upload_image(){
		if(isset($_FILES["image"]["name"])){
			$config['upload_path'] = './assets/img/';
			$config['allowed_types'] = 'jpg|jpeg|png|gif';
			$this->upload->initialize($config);
			if(!$this->upload->do_upload('image')){
				$this->upload->display_errors();
				return FALSE;
			}else{
				$data = $this->upload->data();
		        //Compress Image
		        $config['image_library']='gd2';
		        $config['source_image']='./assets/img/'.$data['file_name'];
		        $config['create_thumb']= FALSE;
	            $config['maintain_ratio']= TRUE;
	            $config['quality']= '60%';
	            $config['width']= 800;
	            $config['height']= 800;
	            $config['new_image']= './assets/img/'.$data['file_name'];
	            $this->load->library('image_lib', $config);
	            $this->image_lib->resize();
				echo base_url().'assets/img/'.$data['file_name'];
			}
		}
	}

	//Delete image summernote
	function delete_image(){
		$src = $this->input->post('src');
		$file_name = str_replace(base_url(), '', $src);
		if(unlink($file_name)){
	        echo 'File Delete Successfully';
	    }
	}


	function act_edit($id){

		$this->form_validation->set_rules('id_karya', 'Karya', 'trim|required');

		$this->form_validation->set_error_delimiters('<span class="help-block">', '</span>');

		if ($this->form_validation->run() == FALSE) {
			 $query = $this->M_artikel->getall($id);
			 $kategori=$this->M_user->get_kategori();
			if ($query->num_rows() > 0) {
				$row = $query->row();
				
				 $data = array(
					'row' => $row,
					'kategori' => $kategori,

				);
				$this->load->view('template/headx');
				$this->load->view('template/header');
				$this->load->view('template/sidebar');
				$this->load->view('artikel/edit_artikel',$data);   
				
			} else {
				echo "<script>alert ('Data tidak ditemukan');";
				echo "window.location ='" . site_url('artikel') . "' ; </scrip>";
			}
		} else {
		$this->M_artikel->edit();
		 if($this->db->affected_rows() > 0){
		$this->session->set_flashdata('info', 'success');
          	 $this->session->set_flashdata('pesan', 'Berhasil merubah data.');
           	redirect('artikel','refresh');
		}
		echo "<script>window.location ='" . site_url('artikel') . "' ; </script>";
		}
	}

public function del()
	{
		$id = $this->input->post('id_karya');
		// $this->m_produksi->del($id);

		$row = $this->M_artikel->get($id);

		if ($row) {
		$this->M_artikel->del($id);
		
		$this->session->set_flashdata('info', 'success');
			$this->session->set_flashdata('pesan', 'Data berhasil dihapus');
			redirect(site_url('artikel'));
		} else {
		$this->session->set_flashdata('info', 'danger');
		$this->session->set_flashdata('pesan', 'gagal hapus ');
		redirect(site_url('artikel'));
		}
	}	


	
}
