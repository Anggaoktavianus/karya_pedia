<!-- Main content -->
<div class="content-wrapper">


    <!-- Page header -->
    <div class="page-header border-bottom-0">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4> Kategori <span class="font-weight-semibold"></span></h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
            <div class="header-elements d-none mb-3 mb-md-0">
                <div class="d-flex justify-content-left">
                    <!-- <a href="#" class="btn btn-link btn-float text-default"><i class="fa fa-plus-square fa-2x"></i><span>Tambah Hak Akses</span></a> -->
                    <!-- <a href="#" class="btn btn-link btn-float text-default"><i class="icon-calculator"></i> <span>Invoices</span></a>
                    <a href="#" class="btn btn-link btn-float text-default"><i class="icon-calendar5"></i> <span>Schedule</span></a> -->
                </div>
            </div>

            <div class="header-elements d-none mb-3 mb-md-0">
                <div class="d-flex justify-content-center">
                    <!-- <a href="#" class="btn btn-link btn-float text-default"><i class="icon-bars-alt"></i><span>Statistics</span></a>
                    <a href="#" class="btn btn-link btn-float text-default"><i class="icon-calculator"></i> <span>Invoices</span></a>
                    <a href="#" class="btn btn-link btn-float text-default"><i class="icon-calendar5"></i> <span>Schedule</span></a>
                </div> -->
                <!-- <a href="<?= site_url('kategori/add')?>" class="btn btn-link btn-float text-default"><i class="fa fa-plus-square fa-2x"></i><span>Buat Kategori Baru</span></a> -->
            </div>
        </div>
    </div>
    <!-- /page header -->
	<!-- Content area -->
    <div class="content pt-0">

            <?php
                $message = $this->session->flashdata('message');
                $info = $this->session->flashdata('info');
                if (isset($message)) { ?>
                    
                    <!-- Solid alert -->
        <div class="alert bg-<?=$info?> alert-styled-left alert-dismissible">
            <button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>
            <span class="font-weight-semibold"><?=strtoupper($info)?>! &nbsp;</span><?=$message?>
        </div>
        <!-- /solid alert -->
        <br/>
            
        <?php 	} ?>

				<!-- Basic initialization -->
        <div class="card">
            <table id="" class="table table-striped datatable-button-html5-basic">
                <form action="" method="Kategori" enctype="multipart/form-data">
                    <div class="modal-body">

                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-12">
                                    <label>Judul</label>
                                    <input type="hidden" name="id_kategori" class="form-control"  value="<?= $row->id_kategori ?>" >
                                    <input type="text"   name="nama_kategori" value="<?= $row->nama_kategori?>" class="form-control">
                                    <?= form_error('nama_kategori', '<div class="text-danger"><small>', '</small></div>') ?>
                                </div>
                            </div>
                        </div>
                        
                         <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
                    </div>

                  
                </form>
            </table>
		</div>
    </div>
</div>

<!-- /content area -->

<?php require(__DIR__ . '/../template/footerx.php') ?>
<script>
//      var markupStr = 'hello world';
// $('#summernote').summernote('code', markupStr);


    </script>
    <script type="text/javascript">
		$(document).ready(function(){
			$('#summernote').summernote({
				height: "300px",
                toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'underline', 'clear']],
                ['fontname', ['fontname']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['table', ['table']],
                ['insert', ['link', 'picture', 'video']],
                ['view', ['fullscreen', 'codeview', 'help']],
                ],
				callbacks: {
			        onImageUpload: function(image) {
			            uploadImage(image[0]);
			        },
			        onMediaDelete : function(target) {
			            deleteImage(target[0].src);
			        }
				}
			});

			function uploadImage(image) {
			    var data = new FormData();
			    data.append("image", image);
			    $.ajax({
			        url: "<?php echo site_url('kategori/upload_image')?>",
			        cache: false,
			        contentType: false,
			        processData: false,
			        data: data,
			        type: "Kategori",
			        success: function(url) {
						$('#summernote').summernote("insertImage", url);
			        },
			        error: function(data) {
			            console.log(data);
			        }
			    });
			}

			function deleteImage(src) {
			    $.ajax({
			        data: {src : src},
			        type: "Kategori",
			        url: "<?php echo site_url('kategori/delete_image')?>",
			        cache: false,
			        success: function(response) {
			            console.log(response);
			        }
			    });
			}

		});
		
	</script>
</body>
</html>