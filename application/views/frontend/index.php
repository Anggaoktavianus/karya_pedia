 <!-- Back to top button -->
<div class="back-to-top"></div>
  <div class="page-section"  id="service">
    <div class="container">
      <div class="row">
         <?php foreach ($row as $key => $data) { ?>
        <div class="col-lg-3">
          <div class="card-service wow fadeInUp">
            <div class="header">
              <img src="<?= base_url('assets/img/services/service-1.svg')?>" alt="">
            </div>
            <div class="body">
              
                <h5 class="text-secondary"><?= $data->nama_kategori?></h5>
                <p>MGMP <?= $data->nama_kategori?> </p>
                <a href="halaman_mapel.html" class="btn btn-primary">Selengkapnya</a>
            </div>
          </div>
        </div>
        <?php } ?>
        <!-- <div class="col-lg-3">
          <div class="card-service wow fadeInUp">
            <div class="header">
              <img src="<?= base_url('assets/img/services/service-2.svg')?>" alt="">
            </div>
            <div class="body">
              <h5 class="text-secondary">Matematika</h5>
              <p>Mata Pelajaran Matematika</p>
              <a href="service.html" class="btn btn-primary">Selengkapnya</a>
            </div>
          </div>
        </div>
        <div class="col-lg-3">
          <div class="card-service wow fadeInUp">
            <div class="header">
              <img src="<?= base_url('assets/img/services/service-3.svg')?>" alt="">
            </div>
            <div class="body">
              <h5 class="text-secondary">Fisika</h5>
              <p>Mata Pelajaran Fisika</p>
              <a href="" class="btn btn-primary">Selengkapnya</a>
            </div>
          </div>
        </div>
        <div class="col-lg-3">
          <div class="card-service wow fadeInUp">
            <div class="header">
              <img src="<?= base_url('assets/img/services/service-3.svg')?>" alt="">
            </div>
            <div class="body">
              <h5 class="text-secondary">Bahasa Indonesia</h5>
              <p>Mata Pelajaran Bahasa Indonesia</p>
              <a href="service.html" class="btn btn-primary">Selengkapnya</a>
            </div>
          </div>
        </div> -->
      </div>
    </div> <!-- .container -->
  </div> <!-- .page-section -->
 

  <!-- <div class="page-section" id="about">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-lg-6 py-3 wow fadeInUp">
          <span class="subhead">About us</span>
          <h2 class="title-section">The number #1 SEO Service Company</h2>
          <div class="divider"></div>

          <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.</p>
          <p>At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren.</p>
          <a href="about.html" class="btn btn-primary mt-3">Read More</a>
        </div>
        <div class="col-lg-6 py-3 wow fadeInRight">
          <div class="img-fluid py-3 text-center">
            <img src="../assets/img/about_frame.png" alt="">
          </div>
        </div>
      </div>
    </div> 
  </div>  -->
  <!-- .page-section -->

  <!-- <div class="page-section bg-light">
    <div class="container">
      <div class="text-center wow fadeInUp">
        <div class="subhead">Our services</div>
        <h2 class="title-section">How SEO Team Can Help</h2>
        <div class="divider mx-auto"></div>
      </div>

        <div class="row">
          <div class="col-sm-6 col-lg-4 col-xl-3 py-3 wow zoomIn">
            <div class="features">
              <div class="header mb-3">
                <span class="mai-business"></span>
              </div>
              <h5>OnSite SEO</h5>
              <p>We analyse your website's structure, internal architecture & other key</p>
            </div>
          </div>
          <div class="col-sm-6 col-lg-4 col-xl-3 py-3 wow zoomIn">
            <div class="features">
              <div class="header mb-3">
                <span class="mai-business"></span>
              </div>
              <h5>OnSite SEO</h5>
              <p>We analyse your website's structure, internal architecture & other key</p>
            </div>
          </div>
          <div class="col-sm-6 col-lg-4 col-xl-3 py-3 wow zoomIn">
            <div class="features">
              <div class="header mb-3">
                <span class="mai-business"></span>
              </div>
              <h5>OnSite SEO</h5>
              <p>We analyse your website's structure, internal architecture & other key</p>
            </div>
          </div>
          <div class="col-sm-6 col-lg-4 col-xl-3 py-3 wow zoomIn">
            <div class="features">
              <div class="header mb-3">
                <span class="mai-business"></span>
              </div>
              <h5>OnSite SEO</h5>
              <p>We analyse your website's structure, internal architecture & other key</p>
            </div>
          </div>
          <div class="col-sm-6 col-lg-4 col-xl-3 py-3 wow zoomIn">
            <div class="features">
              <div class="header mb-3">
                <span class="mai-business"></span>
              </div>
              <h5>OnSite SEO</h5>
              <p>We analyse your website's structure, internal architecture & other key</p>
            </div>
          </div>
          <div class="col-sm-6 col-lg-4 col-xl-3 py-3 wow zoomIn">
            <div class="features">
              <div class="header mb-3">
                <span class="mai-business"></span>
              </div>
              <h5>OnSite SEO</h5>
              <p>We analyse your website's structure, internal architecture & other key</p>
            </div>
          </div>
          <div class="col-sm-6 col-lg-4 col-xl-3 py-3 wow zoomIn">
            <div class="features">
              <div class="header mb-3">
                <span class="mai-business"></span>
              </div>
              <h5>OnSite SEO</h5>
              <p>We analyse your website's structure, internal architecture & other key</p>
            </div>
          </div>
          <div class="col-sm-6 col-lg-4 col-xl-3 py-3 wow zoomIn">
            <div class="features">
              <div class="header mb-3">
                <span class="mai-business"></span>
              </div>
              <h5>OnSite SEO</h5>
              <p>We analyse your website's structure, internal architecture & other key</p>
            </div>
          </div>
        </div>

    </div> 
  </div>  -->
  <!-- .page-section -->

  <!-- <div class="page-section banner-seo-check">
    <div class="wrap bg-image" style="background-image: url(../assets/img/bg_pattern.svg);">
      <div class="container text-center">
        <div class="row justify-content-center wow fadeInUp">
          <div class="col-lg-8">
            <h2 class="mb-4">Check your Website SEO</h2>
            <form action="#">
              <input type="text" class="form-control" placeholder="E.g google.com">
              <button type="submit" class="btn btn-success">Check Now</button>
            </form>
          </div>
        </div>
      </div> 
    </div> 
  </div>  -->
  <!-- .page-section -->

  <!-- <div class="page-section">
    <div class="container">
      <div class="text-center wow fadeInUp">
        <div class="subhead">Pricing Plan</div>
        <h2 class="title-section">Choose plan the right for you</h2>
        <div class="divider mx-auto"></div>
      </div>
      <div class="row mt-5">
        <div class="col-lg-4 py-3 wow zoomIn">
          <div class="card-pricing">
            <div class="header">
              <div class="pricing-type">Basic</div>
              <div class="price">
                <span class="dollar">$</span>
                <h1>39<span class="suffix">.99</span></h1>
              </div>
              <h5>Per Month</h5>
            </div>
            <div class="body">
              <p>25 Analytics <span class="suffix">Campaign</span></p>
              <p>1,300 Change <span class="suffix">Keywords</span></p>
              <p>Social Media <span class="suffix">Reviews</span></p>
              <p>1 Free <span class="suffix">Optimization</span></p>
              <p>24/7 <span class="suffix">Support</span></p>
            </div>
            <div class="footer">
              <a href="#" class="btn btn-pricing btn-block">Subscribe</a>
            </div>
          </div>
        </div>

        <div class="col-lg-4 py-3 wow zoomIn">
          <div class="card-pricing marked">
            <div class="header">
              <div class="pricing-type">Standar</div>
              <div class="price">
                <span class="dollar">$</span>
                <h1>59<span class="suffix">.99</span></h1>
              </div>
              <h5>Per Month</h5>
            </div>
            <div class="body">
              <p>25 Analytics <span class="suffix">Campaign</span></p>
              <p>1,300 Change <span class="suffix">Keywords</span></p>
              <p>Social Media <span class="suffix">Reviews</span></p>
              <p>1 Free <span class="suffix">Optimization</span></p>
              <p>24/7 <span class="suffix">Support</span></p>
            </div>
            <div class="footer">
              <a href="#" class="btn btn-pricing btn-block">Subscribe</a>
            </div>
          </div>
        </div>

        <div class="col-lg-4 py-3 wow zoomIn">
          <div class="card-pricing">
            <div class="header">
              <div class="pricing-type">Professional</div>
              <div class="price">
                <span class="dollar">$</span>
                <h1>99<span class="suffix">.99</span></h1>
              </div>
              <h5>Per Month</h5>
            </div>
            <div class="body">
              <p>25 Analytics <span class="suffix">Campaign</span></p>
              <p>1,300 Change <span class="suffix">Keywords</span></p>
              <p>Social Media <span class="suffix">Reviews</span></p>
              <p>1 Free <span class="suffix">Optimization</span></p>
              <p>24/7 <span class="suffix">Support</span></p>
            </div>
            <div class="footer">
              <a href="#" class="btn btn-pricing btn-block">Subscribe</a>
            </div>
          </div>
        </div>

      </div>
    </div>
  </div> -->
   <!-- .page-section -->

  <!-- Banner info -->
  <!-- <div class="page-section banner-info">
    <div class="wrap bg-image" style="background-image: url(../assets/img/bg_pattern.svg);">
      <div class="container">
        <div class="row align-items-center">
          <div class="col-lg-6 py-3 pr-lg-5 wow fadeInUp">
            <h2 class="title-section">SEO to Improve Brand <br> Visibility</h2>
            <div class="divider"></div>
            <p>We're an experienced and talented team of passionate consultants who breathe with search engine marketing.</p>
            
            <ul class="theme-list theme-list-light text-white">
              <li>
                <div class="h5">SEO Content Strategy</div>
                <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor</p>
              </li>
              <li>
                <div class="h5">B2B SEO</div>
                <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor</p>
              </li>
            </ul>
          </div>
          <div class="col-lg-6 py-3 wow fadeInRight">
            <div class="img-fluid text-center">
              <img src="../assets/img/banner_image_2.svg" alt="">
            </div>
          </div>
        </div>
      </div>
    </div> 
  </div> -->
   <!-- .page-section -->

  <!-- Blog -->
  <div class="page-section" id="karya">
    <div class="container">
      <div class="text-center wow fadeInUp">
        <div class="subhead">Karya</div>
        <h2 class="title-section">Karyatulis </h2>
        <div class="container mt-5">
          <div class="row">
            <div class="col-md-12 ml-auto col-xl-12 mr-auto">
              <!-- Nav tabs -->
              <div class="card">
                <div class="card-header">
                  <ul class="nav nav-tabs justify-content-center" role="tablist">
                    <li class="nav-item">
                      <a class="nav-link active" data-toggle="tab" href="#home" role="tab">
                        <i class="now-ui-icons education_paper"></i> Terbaru
                      </a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" data-toggle="tab" href="#profile" role="tab">
                        <i class="now-ui-icons ui-2_like"></i> Terpopuler
                      </a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" data-toggle="tab" href="#messages" role="tab">
                        <i class="now-ui-icons ui-2_favourite-28"></i> Rekomendasi
                      </a>
                    </li>
                    <!-- <li class="nav-item">
                      <a class="nav-link" data-toggle="tab" href="#settings" role="tab">
                        <i class="now-ui-icons ui-2_settings-90"></i> Settings
                      </a>
                    </li> -->
                  </ul>
                </div>
                <div class="card-body">
                  <!-- Tab panes -->
                  <div class="tab-content text-center">
                    <div class="tab-pane active" id="home" role="tabpanel">
                      <div class="row mt-3">
                        <div class="col-lg-3 py-2 wow fadeInUp">
                          <div class="card-blog">
                            <div class="header">
                              <div class="post-thumb">
                                <img src="<?= base_url('assets/img/blog/blog-2.jpg')?>" alt="" width="100px" height="100px">
                              </div>
                            </div>
                            <div class="body">
                              <h5 class="post-title"><a href="#">Source of Content Inspiration</a></h5>
                            <div class="post-date"><i class="fa fa-calendar"> &nbsp;</i><a href="#">27/02/2020</a> &nbsp;<i class="fa fa-user">&nbsp;</i><a href="">Smith</a>&nbsp;&nbsp;<i class="fa fa-eye">&nbsp;</i><a href="">1000</a><br><i class="fa fa-thumbs-up "></i><a href="">100</a>&nbsp;&nbsp;<i class="fa fa-comment "></i><a href="">100</a>&nbsp; <i class="fa fa-list-alt">&nbsp;<a href="">Kimia</a></i></div>
                            </div>
                          </div>
                        </div>
                        
                        <div class="col-lg-3 py-2 wow fadeInUp">
                          <div class="card-blog">
                            <div class="header">
                              <div class="post-thumb">
                                <img src="<?= base_url('assets/img/blog/blog-2.jpg')?>" alt="" width="100px" height="100px">
                              </div>
                            </div>
                            <div class="body">
                              <h5 class="post-title"><a href="#">Source of Content Inspiration</a></h5>
                                <div class="post-date"><i class="fa fa-calendar"> &nbsp;</i><a href="#">27/02/2020</a> &nbsp;<i class="fa fa-user">&nbsp;</i><a href="">Smith</a>&nbsp;&nbsp;<i class="fa fa-eye">&nbsp;</i><a href="">1000</a><br><i class="fa fa-thumbs-up "></i><a href="">100</a>&nbsp;&nbsp;<i class="fa fa-comment "></i><a href="">100</a>&nbsp; <i class="fa fa-list-alt">&nbsp;<a href="">Kimia</a></i></div>
                            </div>
                          </div>
                        </div>

                        <div class="col-lg-3 py-2 wow fadeInUp">
                          <div class="card-blog">
                            <div class="header">
                              <div class="post-thumb">
                                <img src="<?= base_url('assets/img/blog/blog-2.jpg')?>" alt="" width="100px" height="100px">
                              </div>
                            </div>
                            <div class="body">
                              <h5 class="post-title"><a href="#">Source of Content Inspiration</a></h5>
                              <div class="post-date"><i class="fa fa-calendar"> &nbsp;</i><a href="#">27/02/2020</a> &nbsp;<i class="fa fa-user">&nbsp;</i><a href="">Smith</a>&nbsp;&nbsp;<i class="fa fa-eye">&nbsp;</i><a href="">1000</a><br><i class="fa fa-thumbs-up "></i><a href="">100</a>&nbsp;&nbsp;<i class="fa fa-comment "></i><a href="">100</a>&nbsp; <i class="fa fa-list-alt">&nbsp;<a href="">Kimia</a></i></div>
                            </div>
                          </div>
                        </div>
                        <div class="col-lg-3 py-2 wow fadeInUp">
                          <div class="card-blog">
                            <div class="header">
                              <div class="post-thumb">
                                <img src="<?= base_url('assets/img/blog/blog-2.jpg')?>" alt="" width="100px" height="100px">
                              </div>
                            </div>
                            <div class="body">
                              <h5 class="post-title"><a href="#">Source of Content Inspiration</a></h5>
                                <div class="post-date"><i class="fa fa-calendar"> &nbsp;</i><a href="#">27/02/2020</a> &nbsp;<i class="fa fa-user">&nbsp;</i><a href="">Smith</a>&nbsp;&nbsp;<i class="fa fa-eye">&nbsp;</i><a href="">1000</a><br><i class="fa fa-thumbs-up "></i><a href="">100</a>&nbsp;&nbsp;<i class="fa fa-comment "></i><a href="">100</a>&nbsp; <i class="fa fa-list-alt">&nbsp;<a href="">Kimia</a></i></div>
                            </div>
                          </div>
                        </div>

                        <div class="col-lg-3 py-2 wow fadeInUp">
                          <div class="card-blog">
                            <div class="header">
                              <div class="post-thumb">
                                <img src="<?= base_url('assets/img/blog/blog-3.jpg')?>" alt="" width="10px" height="100px">
                              </div>
                            </div>
                            <div class="body">
                              <h5 class="post-title"><a href="#">Source of Content Inspiration</a></h5>
                                <div class="post-date"><i class="fa fa-calendar"> &nbsp;</i><a href="#">27/02/2020</a> &nbsp;<i class="fa fa-user">&nbsp;</i><a href="">Smith</a>&nbsp;&nbsp;<i class="fa fa-eye">&nbsp;</i><a href="">1000</a><br><i class="fa fa-thumbs-up "></i><a href="">100</a>&nbsp;&nbsp;<i class="fa fa-comment "></i><a href="">100</a>&nbsp; <i class="fa fa-list-alt">&nbsp;<a href="">Kimia</a></i></div>
                            </div>
                          </div>
                        </div>
                        <div class="col-12 mt-4 text-center wow fadeInUp">
                          <nav aria-label="Page navigation example">
                          <ul class="pagination justify-content-center">
                            <li class="page-item">
                              <a class="page-link" href="#" aria-label="Previous">
                                <span aria-hidden="true">&laquo;</span>
                                <span class="sr-only">Previous</span>
                              </a>
                            </li>
                            <li class="page-item"><a class="page-link" href="#">1</a></li>
                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item">
                              <a class="page-link" href="#" aria-label="Next">
                                <span aria-hidden="true">&raquo;</span>
                                <span class="sr-only">Next</span>
                              </a>
                            </li>
                          </ul>
                        </nav>
                          <!-- <a href="blog.html" class="btn btn-primary">View More</a> -->
                        </div>
                        
                      </div>
                    </div>
                    <div class="tab-pane" id="profile" role="tabpanel">
                      <p> I will be the leader of a company that ends up being worth billions of dollars, because I got the answers. I understand culture. I am the nucleus. I think that’s a responsibility that I have, to push possibilities, to show people, this is the level that things could be at. I think that’s a responsibility that I have, to push possibilities, to show people, this is the level that things could be at. </p>
                    </div>
                    <div class="tab-pane" id="messages" role="tabpanel">
                      <p>I think that’s a responsibility that I have, to push possibilities, to show people, this is the level that things could be at. So when you get something that has the name Kanye West on it, it’s supposed to be pushing the furthest possibilities. I will be the leader of a company that ends up being worth billions of dollars, because I got the answers. I understand culture. I am the nucleus.</p>
                    </div>
                    <div class="tab-pane" id="settings" role="tabpanel">
                      <p>
                        "I will be the leader of a company that ends up being worth billions of dollars, because I got the answers. I understand culture. I am the nucleus. I think that’s a responsibility that I have, to push possibilities, to show people, this is the level that things could be at."
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  

  