
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Login Karyapedia</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?=base_url('assets/login/vendor/bootstrap/css/bootstrap.min.css')?>">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?=base_url('assets/login/fonts/font-awesome-4.7.0/css/font-awesome.min.css')?>">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?=base_url('assets/login/fonts/iconic/css/material-design-iconic-font.min.css')?>">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?=base_url('assets/login/vendor/animate/animate.css')?>">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="<?=base_url('assets/login/vendor/css-hamburgers/hamburgers.min.css')?>">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?=base_url('assets/login/vendor/animsition/css/animsition.min.css')?>">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?=base_url('assets/login/vendor/select2/select2.min.css')?>">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="<?=base_url('assets/login/vendor/daterangepicker/daterangepicker.css')?>">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?=base_url('assets/login/css/util.css')?>">
	<link rel="stylesheet" type="text/css" href="<?=base_url('assets/login/css/main.css')?>">
<!--===============================================================================================-->
<style>
    select#category {
   border:0px;
   outline:0px;
}
#hidden_div {
    display: none;
}
</style>
</head>
<body>
	
	
	<div class="container-login100" style="background-image: url('../assets/login/images/bg-01.jpg');">
		<div class="wrap-login100 p-l-55 p-r-55 p-t-80 p-b-30">
			<form class="login100-form validate-form" action="<?= site_url('login/register')?>">
				<span class="login100-form-title p-b-37">
					Sign Up
				</span>
				<div class="row">
					<div class="col-md-7">
                        <div class="wrap-input100 validate-input m-b-20" data-validate="Enter Fullname ">
							<input class="input100" type="text" name="fullname" placeholder="Fullname ">
							<span class="focus-input100"></span>
					    </div>
						
					</div>
					<div class="col-md-5">
                        <div class="wrap-input100 validate-input m-b-20" data-validate="Enter username ">
							<input class="input100" type="text" name="username" placeholder="Username ">
							<span class="focus-input100"></span>
					    </div>
					</div>
                    <div class="col-md-12">
                        <div class="wrap-input100 validate-input m-b-20" data-validate="Enter Email ">
							<input class="input100" type="email" name="email" placeholder="Email ">
							<span class="focus-input100"></span>
					    </div>	
					</div>
                    <div class="col-md-6">
                        <div class="wrap-input100 validate-input m-b-20" data-validate="Enter Password ">
							<input class="input100" type="text" name="username" placeholder="Password ">
							<span class="focus-input100"></span>
					    </div>	
					</div>
                    <div class="col-md-6">
                        <div class="wrap-input100 validate-input m-b-25" data-validate = "Enter confirm password">
                            <input class="input100" type="password" name="pass" placeholder="Confirm password">
                            <span class="focus-input100"></span>
                        </div>
						
					</div>
                    <div class="col-md-4">
                        <div class="wrap-input100 validate-input m-b-25" data-validate = "Category">
                            <!-- <input class="input100" type="password" name="pass" placeholder="Enter Category"> -->
                            <span class="focus-input100">
                                
                            </span>
                            <select name="category" id="category" class="input100" onchange="showDiv('hidden_div', this)">
                                <option value="">--Select--</option>
                                <?php foreach ($role as $key => $data) { ?>
                                <option value="<?= $data->id_role ?>"> <?= $data->nama_role ?></option>
                                <?php } ?>
                            </select>
                        </div>
						
					</div>
                    <div class="col-md-8">
                        <div class="wrap-input100 validate-input m-b-25" data-validate = "Phone">
                            <input class="input100" type="number" name="pass" placeholder="Enter Phone Number">
                            <span class="focus-input100"></span>
                        </div>
						
					</div>
                    <div class="col-md-12" id="hidden_div">
                        <div class="wrap-input100 validate-input m-b-25" data-validate = "Category MGMP">
                            <!-- <input class="input100" type="password" name="pass" placeholder="Enter Category"> -->
                            <span class="focus-input100">
                                
                            </span>
                            <select name="category" id="category" class="input100">
                                <option value="">--Select--</option>
                                 <?php foreach ($row as $key => $data) { ?>
                                <option value="<?= $data->id_kategori ?>"> <?= $data->nama_kategori ?></option>
                                <?php } ?>
                                
                            </select>
                        </div>
						
					</div>
				</div>
				

				<div class="container-login100-form-btn">
					<button class="login100-form-btn">
						Sign Up
					</button>
				</div>

				<div class="text-center p-t-20 p-b-20">
					<span class="txt1">
						
					</span>
				</div>

				<div class="text-center">
					Sudah punya akun?
					<a href="<?= site_url('login')?>" class="txt2 hov1">
						Sign In
					</a>
				</div>
			</form>

			
		</div>
	</div>
	
	

	<div id="dropDownSelect1"></div>
	
<!--===============================================================================================-->
	<script src="<?= base_url('assets/login/vendor/jquery/jquery-3.2.1.min.js')?>"></script>
<!--===============================================================================================-->
	<script src="<?= base_url('assets/login/vendor/animsition/js/animsition.min.js')?>"></script>
<!--===============================================================================================-->
	<script src="<?= base_url('assets/login/vendor/bootstrap/js/popper.js')?>"></script>
	<script src="<?= base_url('assets/login/vendor/bootstrap/js/bootstrap.min.js')?>"></script>
<!--===============================================================================================-->
	<script src="<?= base_url('assets/login/vendor/select2/select2.min.js')?>"></script>
<!--===============================================================================================-->
	<script src="<?= base_url('assets/login/vendor/daterangepicker/moment.min.js')?>"></script>
	<script src="<?= base_url('assets/login/vendor/daterangepicker/daterangepicker.js')?>"></script>
<!--===============================================================================================-->
	<script src="<?= base_url('assets/login/assets/login/vendor/countdowntime/countdowntime.js')?>"></script>
<!--===============================================================================================-->
	<script src="<?= base_url('assets/login/js/main.js')?>"></script>
    <script>
        function showDiv(divId, element)
        {
            document.getElementById(divId).style.display = element.value == 7 ? 'block' : 'none';
        }
    </script>

</body>
</html>