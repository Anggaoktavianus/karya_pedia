<!-- Main content -->
<div class="content-wrapper">
<!-- Page header -->
    <div class="page-header border-bottom-0">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4> Post <span class="font-weight-semibold"></span></h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
            <div class="header-elements d-none mb-3 mb-md-0">
                <div class="d-flex justify-content-left">
                    <!-- <a href="#" class="btn btn-link btn-float text-default"><i class="fa fa-plus-square fa-2x"></i><span>Tambah Hak Akses</span></a> -->
                    <!-- <a href="#" class="btn btn-link btn-float text-default"><i class="icon-calculator"></i> <span>Invoices</span></a>
                    <a href="#" class="btn btn-link btn-float text-default"><i class="icon-calendar5"></i> <span>Schedule</span></a> -->
                </div>
            </div>
            <div class="header-elements d-none mb-3 mb-md-0">
                <div class="d-flex justify-content-center">
                    <!-- <a href="#" class="btn btn-link btn-float text-default"><i class="icon-bars-alt"></i><span>Statistics</span></a>
                    <a href="#" class="btn btn-link btn-float text-default"><i class="icon-calculator"></i> <span>Invoices</span></a>
                    <a href="#" class="btn btn-link btn-float text-default"><i class="icon-calendar5"></i> <span>Schedule</span></a>
                </div> -->
                <a href="<?= site_url('artikel/add')?>" class="btn btn-link btn-float text-default"><i class="fa fa-plus-square fa-2x"></i><span>Buat Post Baru</span></a>
            </div>
        </div>
    </div>
    <!-- /page header -->
	<!-- Content area -->
	<div class="content pt-0">
    <?php
                $message = $this->session->flashdata('pesan');
                $info = $this->session->flashdata('info');
                if (isset($message)) { ?>
                    
                    <!-- Solid alert -->
                    <div class="alert bg-<?=$info?> alert-styled-left alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>
                    <span class="font-weight-semibold"><?=strtoupper($info)?>! &nbsp;</span><?=$message?>
                    </div>
                    <!-- /solid alert -->
                    <br/>
            
            <?php 	} ?>

				<!-- Basic initialization -->
        <div class="card">
            <table id="example" class="table table-striped datatable-button-html5-basic" width="100%">
                <thead style="text-align:left">
                    <tr style="text-align:left">
                        <th style="width:5%">No</th>
                        <th style="width:30%">Judul</th>
                        <th>Kategori</th>
                        <th>Author</th>
                        <th>Tanggal Posting</th>
                        <th style="width:15%"><i class="fas fa-comment">/<i class="fas fa-thumbs-up"></th>
                        <th>Status</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                
                </tbody>
            </table>
        </div>
		<!-- /basic initialization -->
	</div>
	<!-- /content area -->
<?php require(__DIR__ . '/../template/footerx.php') ?>
<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div>
      <div class="modal-body">
        <p>Some text in the modal.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
<script>
/* ------------------------------------------------------------------------------
 *
 *  # Buttons extension for Datatables. HTML5 examples
 *
 *  Demo JS code for datatable_extension_buttons_html5.html page
 *
 * ---------------------------------------------------------------------------- */

var textareaValue = $('.summernote').summernote('code');
  </script>
  <script>
  $(function () {
    $("#example").DataTable({
      "processing": true,
      "searching": true,
      "responsive": false, 
      "lengthChange": true,
      "stateSave": true, 
      "autoWidth": true,
      "serverSide": true,
     
      "ajax": {
                "url": "<?= site_url('artikel/get_ajax') ?>",
                "type": "POST"
            },
      
     dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': $('html').attr('dir') == 'rtl' ? '&larr;' : '&rarr;', 'previous': $('html').attr('dir') == 'rtl' ? '&rarr;' : '&larr;' },
            
        },
                                   
       buttons: {            
            dom: {
                button: {
                    className: 'btn btn-light'
                }
            },
            buttons: [
                'excelHtml5'
            ]
        },
      // "buttons": ["csv", "excel", "pdfHtml5","pageLength"],
      
       
    }).buttons().container().appendTo('#example_wrapper .col-md-6:eq(0)');
    function filterData () {
		    $('#example').DataTable().search(
		        $('.bulan').val()
		    	).draw();
		}
		$('.bulan').on('change', function () {
	        filterData();
	    });

      
  });
</script>

</script>
</body>
</html>

