<!-- Page content -->
<div class="page-content">

    <!-- Main sidebar -->

    <!-- <div class="sidebar sidebar-light sidebar-main sidebar-fixed sidebar-expand-md"> -->
    <div class="sidebar sidebar-light sidebar-main sidebar-fixed sidebar-expand-md">

        <!-- Sidebar mobile toggler -->
        <div class="sidebar-mobile-toggler text-center">
            <a href="#" class="sidebar-mobile-main-toggle">
                <i class="icon-arrow-left8"></i>
            </a>
            Navigation
            <a href="#" class="sidebar-mobile-expand">
                <i class="icon-screen-full"></i>
                <i class="icon-screen-normal"></i>
            </a>
        </div>
        <!-- /sidebar mobile toggler -->


        <!-- Sidebar content -->
        <div class="sidebar-content">

            <!-- User menu -->
            <div class="sidebar-user">
                <div class="card-body">
                    <div class="media">
                        <div class="mr-3">
                            <a href="#"><img src="<?= base_url('assets/icon.png') ?>" width="60" height="60" class="rounded-circle" alt=""></a>
                            <!-- <a href="#"><img src="" width="50" height="60" class="rounded-circle" alt=""></a> -->
                        </div>
                        <div class="media-body">
                            <div class="media-title font-weight-semibold"><?= strtoupper($this->session->userdata('email')) ?></div>
                            <div class="font-size-xs opacity-80">
                                
                            </div>
                        </div>

                        <!-- <div class="ml-3 align-self-center">
                        <a href="#" class="text-white"><i class="icon-cog3"></i></a>
                    </div> -->
                    </div>
                </div>
            </div>
            <!-- /user menu -->


            <!-- Main navigation -->
            <div class="card card-sidebar-mobile">
                <ul class="nav nav-sidebar" data-nav-type="accordion">

                    <!-- Main -->
                    <li class="nav-item-header">
                        <div class="text-uppercase font-size-xs line-height-xs">Main</div> <i class="icon-menu" title="Main"></i>
                    </li>

                    <li class="nav-item">
                        <a href="<?= base_url('dashboard') ?>" class="nav-link">
                            <i class="fa fa-desktop"></i>
                            <span>
                                Dashboard
                            </span>
                        </a>
                    </li>

                    <!-- <li class="nav-item nav-item-submenu <?=active_menu($controller,'setting')?>"> -->
		    <li class="nav-item nav-item-submenu ">
                        <a href="#" class="nav-link"><i class="fa fa-cogs"></i> <span>Setting </span></a>
                        <ul class="nav nav-group-sub" data-submenu-title="Sidebars">
                            <li class="nav-item">
                                <a href="#" class="nav-link ">
                                    <i class="fa fa-cog"></i>
                                    <span>
                                        Konfigurasi
                                    </span>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="<?= base_url('setting/privilege') ?>" class="nav-link ">
                                    <i class="fa fa-users-cog"></i>
                                    <span>
                                        Hak Akses
                                    </span>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="#" class="nav-link ">
                                    <i class="fa fa-sticky-note"></i>
                                    <span>
                                        Note
                                    </span>
                                </a>
                            </li>

                        </ul>
                    </li>

                  

                    <!-- <li class="nav-item nav-item-submenu  <?=active_menu($controller,'izin')?>"> -->
		     <li class="nav-item nav-item-submenu ">
                        <a href="#" class="nav-link"><i class="fa fa-envelope"></i> <span>Karya </span></a>
                        <ul class="nav nav-group-sub" data-submenu-title="Sidebars">
                            <li class="nav-item">
                                <a href="<?= base_url('artikel') ?>" class="nav-link ">
                                    <i class="fa fa-envelope-open-text"></i>
                                    <span>
                                        Post
                                    </span>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="#" class="nav-link ">
                                    <i class="fa fa-mail-bulk"></i>
                                    <span>
                                       Forum
                                    </span>
                                </a>
                            </li>

                            
                        </ul>
                    </li>


                    <!-- <li class="nav-item nav-item-submenu  <?=active_menu($controller,'data_v3')?>"> -->
		     <li class="nav-item nav-item-submenu ">
                        <a href="#" class="nav-link"><i class="fa fa-database"></i> <span>Data Master </span></a>
                        <ul class="nav nav-group-sub" data-submenu-title="Sidebars">
                            <li class="nav-item">
                                <a href="<?= site_url('kategori')?>" class="nav-link ">
                                    <i class="fa fa-list-alt"></i>
                                    <span>
                                    Kategori
                                    </span>
                                </a>
                            </li> 

                            <li class="nav-item">
                                <a href="#" class="nav-link ">
                                    <i class="fa fa-credit-card"></i>
                                    <span>
                                    Paket Langganan
                                    </span>
                                </a>
                            </li> 

                        </ul>
                    </li>




                </ul>
            </div>
            <!-- /main navigation -->

        </div>
        <!-- /sidebar content -->

    </div>
    <!-- /main sidebar -->