<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Karyapedia</title>
	<!-- App Icons -->
	<link rel="shortcut icon" href="<?=base_url()?>">

	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="<?=base_url()?>assets/limitless/global_assets/css/icons/icomoon/styles.min.css" rel="stylesheet" type="text/css">
	<link href="<?=base_url()?>assets/limitless/global_assets/css/icons/fontawesome/styles.min.css" rel="stylesheet" type="text/css">
	<link href="<?=base_url()?>assets/limitless/light/full/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="<?=base_url()?>assets/limitless/light/full/assets/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
	<link href="<?=base_url()?>assets/limitless/light/full/assets/css/layout.min.css" rel="stylesheet" type="text/css">
	<link href="<?=base_url()?>assets/limitless/light/full/assets/css/components.min.css" rel="stylesheet" type="text/css">
	<link href="<?=base_url()?>assets/limitless/light/full/assets/css/colors.min.css" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script src="<?=base_url()?>assets/limitless/global_assets/js/main/jquery.min.js"></script>
	<script src="<?=base_url()?>assets/limitless/global_assets/js/main/bootstrap.bundle.min.js"></script>
	<script src="<?=base_url()?>assets/limitless/global_assets/js/plugins/loaders/blockui.min.js"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script src="<?=base_url()?>assets/limitless/global_assets/js/plugins/visualization/d3/d3.min.js"></script>
	<script src="<?=base_url()?>assets/limitless/global_assets/js/plugins/visualization/d3/d3_tooltip.js"></script>
	<script src="<?=base_url()?>assets/limitless/global_assets/js/plugins/forms/styling/switchery.min.js"></script>
	<script src="<?=base_url()?>assets/limitless/global_assets/js/plugins/ui/moment/moment.min.js"></script>
	<script src="<?=base_url()?>assets/limitless/global_assets/js/plugins/pickers/daterangepicker.js"></script>

	<script src="<?=base_url()?>assets/limitless/light/full/assets/js/app.js"></script>
	<script src="<?=base_url()?>assets/limitless/global_assets/js/demo_pages/dashboard.js"></script>
    <script src="<?=base_url()?>assets/limitless/global_assets/js/demo_pages/layout_fixed_sidebar_custom.js"></script>

    <!-- Theme JS files Form -->
	<script src="<?=base_url()?>assets/limitless/global_assets/js/plugins/extensions/jquery_ui/interactions.min.js"></script>
	<script src="<?=base_url()?>assets/limitless/global_assets/js/plugins/forms/selects/select2.min.js"></script>
	<script src="<?=base_url()?>assets/limitless/global_assets/js/demo_pages/form_select2.js"></script>
	<script src="<?=base_url()?>assets/limitless/global_assets/js/plugins/forms/styling/uniform.min.js"></script>
	<script src="<?=base_url()?>assets/limitless/global_assets/js/demo_pages/form_inputs.js"></script>
	<!-- /theme JS files Form -->
   
    <!-- Theme JS files Table -->
	<script src="<?=base_url()?>assets/limitless/global_assets/js/plugins/forms/selects/select2.min.js"></script>
	<!-- <link href="https:////cdn.datatables.net/1.10.25/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css">
	<script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script> -->
	

    <script src="<?=base_url()?>assets/limitless/global_assets/js/plugins/tables/datatables/datatables.min.js"></script>
	
	<script src="<?=base_url()?>assets/limitless/global_assets/js/plugins/tables/datatables/extensions/jszip/jszip.min.js"></script>
	<script src="<?=base_url()?>assets/limitless/global_assets/js/plugins/tables/datatables/extensions/pdfmake/pdfmake.min.js"></script>
	<script src="<?=base_url()?>assets/limitless/global_assets/js/plugins/tables/datatables/extensions/pdfmake/vfs_fonts.min.js"></script>
    <script src="<?=base_url()?>assets/limitless/global_assets/js/plugins/tables/datatables/extensions/buttons.min.js"></script>
    <!-- <script src="assets/limitless/full/assets/global_assets/js/demo_pages/datatables_extension_buttons_html5.js"></script> -->
	<!-- <script src="assets/datatables.js"></script>  -->

	<style>
	.center{
		text-align:center
	}
	</style>

	<script> 
		var site = "<?php echo site_url()?>";
	</script>

	

</head>

<!-- <?php include('sesi.php');?> -->

<!-- <body class="navbar-top sidebar-xs"> -->
<body class="navbar-top"> 

	<!-- Main navbar -->
	<div class="navbar navbar-expand-md navbar-light fixed-top">
		<div class="navbar-brand">
			<a href="<?=base_url('dashboard')?>" class="d-inline-block">
				<img src="<?=base_url()?>assets/presensi_black.png" alt="" width="90px" >
			</a> 
			
		</div>

		<div class="d-md-none">
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-mobile">
				<i class="icon-tree5"></i>
			</button>
			<button class="navbar-toggler sidebar-mobile-main-toggle" type="button">
				<i class="icon-paragraph-justify3"></i>
			</button>
		</div>

		<div class="collapse navbar-collapse" id="navbar-mobile">
			<ul class="navbar-nav">
				<li class="nav-item">
					<a href="#" class="navbar-nav-link sidebar-control sidebar-main-toggle d-none d-md-block">
						<i class="icon-paragraph-justify3"></i>
					</a>
				</li>

				
			</ul>

			<span class="badge bg-success my-3 my-md-0 ml-md-3 mr-md-auto">Online</span>

			<ul class="navbar-nav">
				
					<li class="nav-item dropdown dropdown-user">
					<a href="#" class="navbar-nav-link d-flex align-items-center dropdown-toggle" data-toggle="dropdown">
						<img src="<?=base_url()?>assets/jateng.png" class="rounded-circle mr-2" height="34" alt="">
						<!-- <span><?=$this->formatter->getDateTimeFormatUser(date('Y-m-d H:i:s'))?></span> -->
					</a>

					<div class="dropdown-menu dropdown-menu-right">
						<!-- <a href="#" class="dropdown-item"><i class="icon-user-plus"></i> My profile</a>
						<a href="#" class="dropdown-item"><i class="icon-coins"></i> My balance</a>
						<a href="#" class="dropdown-item"><i class="icon-comment-discussion"></i> Messages <span class="badge badge-pill bg-blue ml-auto">58</span></a>
						<div class="dropdown-divider"></div>
						<a href="#" class="dropdown-item"><i class="icon-cog5"></i> Account settings</a> -->
						<a href="#" class="dropdown-item" data-toggle="modal" data-target="#modal_default"><i class="icon-user"></i> Profil Saya</a>
						<a href="<?=base_url('portal')?>" class="dropdown-item"><i class="icon-switch2"></i> Keluar</a>
					</div>
				</li>
			</ul>
		</div>
	</div>
	<!-- /main navbar -->

	<!-- Basic modal -->
	<div id="modal_default" class="modal fade" tabindex="-1">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title">Profil Saya</h5>
								<button type="button" class="close" data-dismiss="modal">&times;</button>
							</div>

							<div class="modal-body ">
							<!-- <table>
								<tr>
								<td>
								<img src="<?=$urlFoto?>" width="100" height="130" alt="">
								</td>
								<td>&nbsp;&nbsp;&nbsp;</td>
								<td>
								<table width="100%" style="padding:10px">
									<tr><td>Nama Lengkap</td><td>:</td><td> <?=$dataPns->B_03A?> <?=$dataPns->B_03?> <?=$dataPns->B_03B?></td></tr>
									<tr><td>NIP</td><td>:</td><td> <?=$dataPns->B_02B?></td></tr>
									<tr><td>Tempat, Tanggal Lahir</td><td>:</td><td> <?=$dataPns->B_04?>, <?=$this->formatter->getDateMonthFormatUser($dataPns->B_05)?></td></tr>
									<tr><td>Jabatan</td><td>:</td><td> <?=$dataPns->I_JB?></td></tr>
									<tr><td>Lokasi Kerja</td><td>:</td><td> <?=$lokasiKerja->NALOKP?></td></tr>
									<tr><td>Unit Kerja</td><td>:</td><td> <?=$unitKerja->NALOKP?></td></tr>	
								</table>
								</td>
								
								</tr>


							</table> -->
								
							</div>

							<div class="modal-footer">
								<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
								<!-- <button type="button" class="btn bg-primary">Save changes</button> -->
							</div>
						</div>
					</div>
				</div>
				<!-- /basic modal -->

<!-- Page content -->
<div class="page-content">

    <!-- Main sidebar -->

    <!-- <div class="sidebar sidebar-light sidebar-main sidebar-fixed sidebar-expand-md"> -->
    <div class="sidebar sidebar-light sidebar-main sidebar-fixed sidebar-expand-md">

        <!-- Sidebar mobile toggler -->
        <div class="sidebar-mobile-toggler text-center">
            <a href="#" class="sidebar-mobile-main-toggle">
                <i class="icon-arrow-left8"></i>
            </a>
            Navigation
            <a href="#" class="sidebar-mobile-expand">
                <i class="icon-screen-full"></i>
                <i class="icon-screen-normal"></i>
            </a>
        </div>
        <!-- /sidebar mobile toggler -->


        <!-- Sidebar content -->
        <div class="sidebar-content">

            <!-- User menu -->
            <div class="sidebar-user">
                <div class="card-body">
                    <div class="media">
                        <div class="mr-3">
                            <!-- <a href="#"><img src="<?= base_url() ?>assets/jateng.png" width="60" height="60" class="rounded-circle" alt=""></a> -->
                            <a href="#"><img src="" width="50" height="60" class="rounded-circle" alt=""></a>
                        </div>

                        <div class="media-body">
                            <div class="media-title font-weight-semibold"><?= strtoupper($this->session->userdata('email')) ?></div>
                            <div class="font-size-xs opacity-80">
                                <!-- <?=$dataPns->B_03?> <br />
                                <u><?=$dataPns->B_02B?></u>
                                <br />
                                <?= $roleUser ?> -->
                            </div>
                        </div>

                        <!-- <div class="ml-3 align-self-center">
                        <a href="#" class="text-white"><i class="icon-cog3"></i></a>
                    </div> -->
                    </div>
                </div>
            </div>
            <!-- /user menu -->


            <!-- Main navigation -->
            <div class="card card-sidebar-mobile">
                <ul class="nav nav-sidebar" data-nav-type="accordion">

                    <!-- Main -->
                    <li class="nav-item-header">
                        <div class="text-uppercase font-size-xs line-height-xs">Main</div> <i class="icon-menu" title="Main"></i>
                    </li>
                    <!-- <li class="nav-item">
                    <a href="<?= base_url('dashboard') ?>" class="nav-link active">
                        <i class="icon-home4"></i>
                        <span>
                            Kota
                        </span>
                    </a>
                </li>
                

                <li class="nav-item">
                    <a href="tablex.html" class="nav-link">
                        <i class="icon-design"></i>
                        <span>
                            Table
                        </span>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="formx.html" class="nav-link">
                        <i class="icon-pencil5"></i>
                        <span>
                            Form
                        </span>
                    </a>
                </li> -->



                    <li class="nav-item">
                        <a href="<?= base_url('dashboard') ?>" class="nav-link">
                            <i class="fa fa-desktop"></i>
                            <span>
                                Dashboard
                            </span>
                        </a>
                    </li>

                    <!-- <li class="nav-item nav-item-submenu <?=active_menu($controller,'setting')?>"> -->
		    <li class="nav-item nav-item-submenu ">
                        <a href="#" class="nav-link"><i class="fa fa-cogs"></i> <span>Setting </span></a>
                        <ul class="nav nav-group-sub" data-submenu-title="Sidebars">
                            <li class="nav-item">
                                <a href="<?= base_url('setting/konfigurasi') ?>" class="nav-link ">
                                    <i class="fa fa-cog"></i>
                                    <span>
                                        Konfigurasi
                                    </span>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="<?= base_url('setting/privilege') ?>" class="nav-link ">
                                    <i class="fa fa-users-cog"></i>
                                    <span>
                                        Hak Akses
                                    </span>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="<?= base_url('setting/note') ?>" class="nav-link ">
                                    <i class="fa fa-sticky-note"></i>
                                    <span>
                                        Note
                                    </span>
                                </a>
                            </li>

                        </ul>
                    </li>

                    <li class="nav-item nav-item-submenu <?=active_menu($controller,'jadwal')?>">
                        <a href="#" class="nav-link"><i class="fa fa-calendar-alt"></i> <span>Jadwal </span></a>
                        <ul class="nav nav-group-sub" data-submenu-title="Sidebars">
                            <li class="nav-item">
                                <a href="<?= base_url('jadwal/saya') ?>" class="nav-link ">
                                    <i class="fa fa-clipboard-list"></i>
                                    <span>
                                        Jadwal Saya
                                    </span>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="<?= base_url('jadwal/libur') ?>" class="nav-link ">
                                    <i class="fa fa-calendar-times"></i>
                                    <span>
                                        Hari Libur
                                    </span>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="<?= base_url('jadwal/jam') ?>" class="nav-link ">
                                    <i class="fa fa-clock"></i>
                                    <span>
                                        Jam Kerja
                                    </span>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="<?= base_url('jadwal/kerja') ?>" class="nav-link ">
                                    <i class="fa fa-calendar-check"></i>
                                    <span>
                                        Jadwal Kerja
                                    </span>
                                </a>
                            </li>

                        </ul>
                    </li>

                    <li class="nav-item nav-item-submenu  <?=active_menu($controller,'izin')?>">
                        <a href="#" class="nav-link"><i class="fa fa-envelope"></i> <span>Izin </span></a>
                        <ul class="nav nav-group-sub" data-submenu-title="Sidebars">
                            <li class="nav-item">
                                <a href="<?= base_url('izin/saya') ?>" class="nav-link ">
                                    <i class="fa fa-envelope-open-text"></i>
                                    <span>
                                        Izin Saya
                                    </span>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="<?= base_url('izin/data') ?>" class="nav-link ">
                                    <i class="fa fa-mail-bulk"></i>
                                    <span>
                                        Data Izin
                                    </span>
                                </a>
                            </li>

                            
                        </ul>
                    </li>

                    <!-- <li class="nav-item nav-item-submenu <?=active_menu($controller,'data')?>" >
                        <a href="#" class="nav-link"><i class="fa fa-database"></i> <span>Data </span></a>
                        <ul class="nav nav-group-sub" data-submenu-title="Sidebars">
                            <li class="nav-item">
                                <a href="<?= base_url('data/presensi') ?>" class="nav-link ">
                                    <i class="fa fa-fingerprint"></i>
                                    <span>
                                        Presensi
                                    </span>
                                </a>
                            </li> 
                        </ul>
                    </li> -->

                    <li class="nav-item nav-item-submenu  <?=active_menu($controller,'data_v3')?>">
                        <a href="#" class="nav-link"><i class="fa fa-database"></i> <span>Data </span></a>
                        <ul class="nav nav-group-sub" data-submenu-title="Sidebars">
                            <li class="nav-item">
                                <a href="<?= base_url('data_v3/presensi') ?>" class="nav-link ">
                                    <i class="fa fa-fingerprint"></i>
                                    <span>
                                    Presensi
                                    </span>
                                </a>
                            </li> 

                            <li class="nav-item">
                                <a href="<?= base_url('data_v3/presensi/all') ?>" class="nav-link ">
                                    <i class="fa fa-fingerprint"></i>
                                    <span>
                                    Bulk Save 
                                    </span>
                                </a>
                            </li> 

                        </ul>
                    </li>



                    <li class="nav-item nav-item-submenu  <?=active_menu($controller,'rekap')?>">
                        <a href="#" class="nav-link"><i class="fa fa-file-alt"></i> <span>Rekap Absen </span></a>
                        <ul class="nav nav-group-sub" data-submenu-title="Sidebars">
                            <li class="nav-item">
                                <a href="<?= base_url('rekap/saya') ?>" class="nav-link ">
                                    <i class="fa fa-user"></i>
                                    <span>
                                         Saya
                                    </span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?= base_url('rekap/personal') ?>" class="nav-link ">
                                    <i class="fa fa-user"></i>
                                    <span>
                                         Personal
                                    </span>
                                </a>
                            </li> 
                            <li class="nav-item">
                                <a href="<?= base_url('rekap/skpd') ?>" class="nav-link ">
                                    <i class="fa fa-user"></i>
                                    <span>
                                         SKPD
                                    </span>
                                </a>
                            </li> 

                            <li class="nav-item">
                                <a href="<?= base_url('rekap/bulanan') ?>" class="nav-link ">
                                    <i class="fa fa-user"></i>
                                    <span>
                                         Bulanan
                                    </span>
                                </a>
                            </li> 

                            <li class="nav-item">
                                <a href="<?= base_url('rekap/progress') ?>" class="nav-link ">
                                    <i class="fa fa-user"></i>
                                    <span>
                                         Mangkir
                                    </span>
                                </a>
                            </li> 

                            <li class="nav-item">
                                <a href="<?= base_url('rekap/progress') ?>" class="nav-link ">
                                    <i class="fa fa-user"></i>
                                    <span>
                                    Lebih Dari 28
                                    </span>
                                </a>
                            </li> 
                        </ul>
                    </li>

                    <li class="nav-item">
                        <a href="<?= base_url('manual') ?>" class="nav-link ">
                            <i class="fa fa-book-reader"></i>
                            <span>
                                Manual Book
                            </span>
                        </a>
                    </li>

                    



                    <!-- <li class="nav-item nav-item-submenu nav-item-expanded nav-item-open ">
							<a href="#" class="nav-link"><i class="icon-office"></i> <span>Setting </span></a>
							<ul class="nav nav-group-sub" data-submenu-title="Sidebars">
                                <li class="nav-item">
                                <a href="<?= base_url('p') ?>/tambah/ /reguler" class="nav-link ">
                                    <i class="icon-office"></i>
                                    <span>
                                        Konfigurasi
                                </span>
                                </a>
                                </li>

                                <li class="nav-item">
                                <a href="<?= base_url('p') ?>/tambah/ /reguler" class="nav-link ">
                                    <i class="icon-office"></i>
                                    <span>
                                        Hak Akses
                                </span>
                                </a>
                                </li>

                                <li class="nav-item">
                                <a href="<?= base_url('p') ?>/tambah/ /reguler" class="nav-link ">
                                    <i class="icon-office"></i>
                                    <span>
                                        Note
                                </span>
                                </a>
                                </li>


                            
								 <li class="nav-item nav-item-submenu nav-item-expanded nav-item-open">
									<a href="#" class="nav-link"><i class="icon-office"></i>Tambah Data</a>
									<ul class="nav nav-group-sub">
                                       
										<li class="nav-item"><a href="<?= base_url('p') ?>/tambah/ /reguler" class="nav-link active">Reguler</a></li>
                                        <li class="nav-item"><a href="<?= base_url('p') ?>/tambah/ /aspirasi" class="nav-link ">Aspirasi</a></li>
                                    
									</ul>
                                </li> 
                        
								
							</ul>
						</li>  -->




















                    <!-- <li class="nav-item nav-item-submenu">
							<a href="#" class="nav-link"><i class="icon-tree5"></i> <span>Menu levels</span></a>
							<ul class="nav nav-group-sub" data-submenu-title="Menu levels">
								
								<li class="nav-item nav-item-submenu">
									<a href="#" class="nav-link"><i class="icon-firefox"></i> Second level with child</a>
									<ul class="nav nav-group-sub">
										
										<li class="nav-item nav-item-submenu">
											<a href="#" class="nav-link"><i class="icon-apple2"></i> Third level with child</a>
											<ul class="nav nav-group-sub">
												<li class="nav-item"><a href="#" class="nav-link"><i class="icon-html5"></i> Fourth level</a></li>
												<li class="nav-item"><a href="#" class="nav-link"><i class="icon-css3"></i> Fourth level</a></li>
											</ul>
										</li>
										
									</ul>
								</li>
								
							</ul>
						</li> -->

                    <!-- /main -->

                    <!-- Forms -->
                    <!-- <li class="nav-item-header"><div class="text-uppercase font-size-xs line-height-xs">Forms</div> <i class="icon-menu" title="Forms"></i></li>
                <li class="nav-item nav-item-submenu">
                    <a href="#" class="nav-link"><i class="icon-pencil3"></i> <span>Form components</span></a>
                    <ul class="nav nav-group-sub" data-submenu-title="Form components">
                        <li class="nav-item"><a href="form_inputs.html" class="nav-link">Basic inputs</a></li>
                        <li class="nav-item"><a href="form_checkboxes_radios.html" class="nav-link">Checkboxes &amp; radios</a></li>
                        
                    </ul>
                </li> -->


                    <!-- /forms -->





                </ul>
            </div>
            <!-- /main navigation -->

        </div>
        <!-- /sidebar content -->

    </div>
    <!-- /main sidebar -->