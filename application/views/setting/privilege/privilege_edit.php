<!-- Main content -->
<div class="content-wrapper">

    <!-- Page header -->
    <div class="page-header border-bottom-0">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4> Setting - <span class="font-weight-semibold">Hak Akses</h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>

            <div class="header-elements d-none mb-3 mb-md-0">
                <!-- <div class="d-flex justify-content-center">
							<a href="#" class="btn btn-link btn-float text-default"><i class="icon-bars-alt"></i><span>Statistics</span></a>
							<a href="#" class="btn btn-link btn-float text-default"><i class="icon-calculator"></i> <span>Invoices</span></a>
							<a href="#" class="btn btn-link btn-float text-default"><i class="icon-calendar5"></i> <span>Schedule</span></a>
						</div> -->
            </div>
        </div>
    </div>
    <!-- /page header -->


    <!-- Content area -->
    <div class="content pt-0">

        <?php
         $this->load->model('Privilege_model');
        $message = $this->session->flashdata('message');
        $info = $this->session->flashdata('info');
        if (isset($message)) { ?>

            <!-- Solid alert -->
            <div class="alert bg-<?= $info ?> alert-styled-left alert-dismissible">
                <button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>
                <span class="font-weight-semibold"><?= strtoupper($info) ?>! &nbsp;</span><?= $message ?>
            </div>
            <!-- /solid alert -->
            <br />

        <?php     } ?>

        <!-- Form inputs -->
        <div class="card col-lg-10">


            <div class="card-body ">
                <!-- <form action="#"> -->
                <?php 
                $id = $this->uri->segment(3);
                echo form_open('setting/privilege_update/'.$id, 'id="form"') ?>

                

                <div class="form-group row">
                        <table class="table table-striped datatable-button-html5-basic">
                            <tr>
                             
                                <td><input type="hidden" width="100%" required id="id" name="id" value="<?php echo $form_data['id'] ?>" class="form-control"><label class="col-form-label "><?php echo $form_data['id'] ?></label></td>
                                <td><input type="text" width="100%" required id="name" name="name" value="<?php echo $form_data['name'] ?>" class="form-control"></td>
                                <td><input type="text" width="100%" required id="description" name="description" value="<?php echo $form_data['description'] ?>" class="form-control"></td>

                            </tr>
                        </table>
                       
                </div>

 
		
<div class="row">		

<div class="col s6 switch">
    <label>
      Semua Modul
      <input type="checkbox" name="all" id="all" onclick="allAccess()" <?php if($form_data['all']=='true') echo 'checked="checked"' ?> value="true">
      <span class="lever"></span>
      
    </label>
  </div>
 
 <div class="col s6 switch">

 <label>
      Aktif
      <input type="checkbox"  id="status" name="status" value="1" <?php if($form_data['status']=='1') echo 'checked="checked"' ?>>
      <span class="lever"></span>
      
    </label>
		  
	
</div>
</div>

<ul class="tabs">
	<?php $a = 1;				
	foreach($module_data->result() as $module)
	{ ?>
	
	<?php $a++; } ?>	
	
</ul>
<table class="table table-striped datatable-button-html5-basic">
<thead>
<tr>
<th class="center">No</th>
<th class="center">Akses</th>
<th class="center">Modul</th>


</tr>
<thead>
	<?php $b = 1;				
	foreach($module_data->result() as $modules) { ?>
<div id="tab_<?php echo $b?>" class="col s12">

<thead>
<tr>
<th class="left" colspan=4><?php echo $modules->name ?></th>


</tr>

<tbody>
<tr>
<td class="right" width="5%">#</td>
<td class="center" width="10%">
<p class="center">
      <input type="checkbox" class="all" data="<?php echo $modules->id?> <?php echo $modules->child?>"  <?php if(in_array($modules->id,$access)) echo 'checked="checked"' ?> name="module[]" id="<?php echo $modules->id ?>" onclick="access('<?php echo $modules->id?>')" value="<?php echo $modules->id?>"/>
      <label for="<?php echo $modules->id ?>"></label>
    </p>	
	
	</td>
<td><?php echo $modules->name ?></td>

</tr>	
<?php
$no = 1;
foreach($this->Privilege_model->get_kp_module('',$modules->id)->result() as $module) {
?>
<tr>
<td class="right" width="5%"><?php echo $no?> </td>
<td class="center" width="10%">

	<p class="center">
      <input type="checkbox" class="all" data="<?php echo $module->id?> <?php echo $module->child?>"  <?php if(in_array($module->id,$access)) echo 'checked="checked"' ?> name="module[]" id="<?php echo $module->id ?>" onclick="access('<?php echo $module->id?>')" value="<?php echo $module->id?>"/>
      <label for="<?php echo $module->id ?>"></label>
    </p>	
	
	</td>
<td><?php for($a=1;$a<=$module->level;$a++){ echo '__'; } ?><?php echo $module->name ?></td>

</tr>	
	
<?php $no++; } ?>
</tbody>
	
	

	</div>
  <?php $b++; } ?>
  </table>
  


                <div class="text-left">
               
                    <button type="submit" class="btn btn-primary"> <i class="fa fa-save"></i> Simpan </button>
                </div>
                <?php echo form_close() ?>
            </div>
        </div>
        <!-- /form inputs -->

    </div>
    <!-- /content area -->

    <?php require(__DIR__ . '/../../template/footerx.php') ?>

    <script>
        $(function() {
            //Initialize Select2 Elements
            $('.select2').select2()
        });
    </script>

    <script>
        function allAccess()
{
	if($('#all').prop('checked')) {
   	$(".all").prop('checked', true);	
	} else {
		$(".all").prop('checked', false);
	}
	
}

function access(id)
{
	if($('#'+id).prop('checked')) {
    data = $("#"+id).attr("data").split(' ');
		for(var i=0;i<data.length;i++)
		{
		$("#"+data[i]).prop('checked', true);
		}
	} else {
	data = $("#"+id).attr("data").split(' ');
		for(var i=0;i<data.length;i++)
		{
		$("#all").prop('checked', false);
		$("#"+data[i]).prop('checked', false);
		}
	}
	
}


function simpan()
{
	$.ajax({
        url: '<?=base_url('setting/privilege_update/'.$this->uri->segment(3))?>', 
        data: $('#form').serialize(),
		beforeSend: function() {
		progressbar('open');
		},
		success: function(result){
			// progressbar('close');
			// if (result.success)
			// {
			// messager('success',result.message);
			// window.location.replace(site+'/setting/privilege');
			// } 
			// else 
			// {
			// messager('error',result.message);
			// }
            window.location.replace('<?=base_url('setting/privilege_edit/'.$this->uri->segment(3))?>');
			
            },
        type: "post", 
       dataType: "json"
    }); 
	
}

    </script>


</div>
<!-- /main content -->

</div>
<!-- /page content -->

</body>
</html>